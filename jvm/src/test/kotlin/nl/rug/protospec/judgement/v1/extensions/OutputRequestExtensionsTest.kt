package nl.rug.protospec.judgement.v1.extensions

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.common.kotlin.quantities.KB
import nl.rug.protospec.judgement.v1.OutputRequest
import nl.rug.protospec.judgement.v1.OutputRequestKt
import nl.rug.protospec.judgement.v1.outputRequest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.net.URI

@QuarkusTest
class OutputRequestExtensionsTest {
    @Test
    fun `The output request maxSize ByteSize representation getter works as expected`() {
        val request = outputRequest {
            maxSizeBytes = 10_000
        }

        assertEquals(10.KB, request.maxSize)
    }

    @Test
    fun `The output request maxSize ByteSize representation setter works as expected`() {
        val request = outputRequest {
            maxSize = 10.KB
        }

        assertEquals(10_000, request.maxSizeBytes)
    }

    @Test
    fun `The output request uri URI representation getter works as expected`() {
        val request = outputRequest {
            uriString = "file://root.txt"
        }

        assertEquals(
            URI("file://root.txt"),
            request.uri
        )
    }

    @Test
    fun `The output request uri URI representation DSL getter works as expected`() {
        val request = OutputRequestKt.Dsl._create(
            OutputRequest.newBuilder().setUriString("file://root.txt")
        )

        assertEquals(
            URI("file://root.txt"),
            request.uri
        )
    }

    @Test
    fun `The output request uri URI representation setter works as expected`() {
        val request = outputRequest {
            uri = URI("file://root.txt")
        }

        assertEquals(
            "file://root.txt",
            request.uriString
        )
    }
}

package nl.rug.protospec.judgement.v1.extensions

import io.quarkus.test.junit.QuarkusTest
import nl.rug.protospec.judgement.v1.Resource
import nl.rug.protospec.judgement.v1.ResourceKt
import nl.rug.protospec.judgement.v1.resource
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.net.URI

@QuarkusTest
class ResourceExtensionsTest {
    @Test
    fun `The output request uri URI representation getter works as expected`() {
        // Default getter
        val resource = resource {
            uriString = "file://root.txt"
        }

        assertEquals(
            URI("file://root.txt"),
            resource.uri
        )
    }

    @Test
    fun `The output request uri URI representation DSL getter works as expected`() {
        // Kotlin DSL getter (This is barely ever used by the user, but is available)
        val dslResource = ResourceKt.Dsl._create(
            Resource.newBuilder().setUriString("file://root.txt")
        )

        assertEquals(
            URI("file://root.txt"),
            dslResource.uri
        )
    }

    @Test
    fun `The output request uri URI representation setter works as expected`() {
        val resource = resource {
            uri = URI("file://root.txt")
        }

        assertEquals(
            "file://root.txt",
            resource.uriString
        )
    }
}

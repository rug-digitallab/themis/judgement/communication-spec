package nl.rug.protospec.judgement.v1.extensions

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.common.kotlin.quantities.KB
import nl.rug.protospec.judgement.v1.executionLimits
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import kotlin.time.Duration.Companion.seconds

@QuarkusTest
class ExecutionLimitsExtensionsTest {
    @Test
    fun `The limits wallTime Duration representation getter works as expected`() {
        val limits = executionLimits {
            wallTimeMs = 10_000
        }

        assertEquals(10.seconds, limits.wallTime)
    }

    @Test
    fun `The limits wallTime Duration representation setter works as expected`() {
        val limits = executionLimits {
            wallTime = 10.seconds
        }

        assertEquals(10_000, limits.wallTimeMs)
    }

    @Test
    fun `The limits memory ByteSize representation getter works as expected`() {
        val limits = executionLimits {
            memoryBytes = 10_000
        }

        assertEquals(10.KB, limits.memory)
    }

    @Test
    fun `The limits memory ByteSize representation setter works as expected`() {
        val limits = executionLimits {
            memory = 10.KB
        }

        assertEquals(10_000, limits.memoryBytes)
    }

    @Test
    fun `The limits disk ByteSize representation getter works as expected`() {
        val limits = executionLimits {
            diskBytes = 10_000
        }

        assertEquals(10.KB, limits.disk)
    }

    @Test
    fun `The limits disk ByteSize representation setter works as expected`() {
        val limits = executionLimits {
            disk = 10.KB
        }

        assertEquals(10_000, limits.diskBytes)
    }

    @Test
    fun `The limits ioStreams ByteSize representation getter works as expected`() {
        val limits = executionLimits {
            outputBytes = 10_000
        }

        assertEquals(10.KB, limits.output)
    }

    @Test
    fun `The limits ioStreams ByteSize representation setter works as expected`() {
        val limits = executionLimits {
            output = 10.KB
        }

        assertEquals(10_000, limits.outputBytes)
    }
}

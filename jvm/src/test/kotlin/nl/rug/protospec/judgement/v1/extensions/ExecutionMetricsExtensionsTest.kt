package nl.rug.protospec.judgement.v1.extensions

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.common.kotlin.quantities.KB
import nl.rug.protospec.judgement.v1.ExecutionMetrics
import nl.rug.protospec.judgement.v1.ExecutionMetricsKt
import nl.rug.protospec.judgement.v1.executionMetrics
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import kotlin.time.Duration.Companion.seconds

@QuarkusTest
class ExecutionMetricsExtensionsTest {
    @Test
    fun `The metrics wallTime Duration representation getter works as expected`() {
        val metrics = executionMetrics {
            wallTimeMs = 10_000
        }

        assertEquals(10.seconds, metrics.wallTime)
    }

    @Test
    fun `The metrics wallTime Duration representation DSL getter works as expected`() {
        val metrics = ExecutionMetricsKt.Dsl._create(
            ExecutionMetrics.newBuilder().setWallTimeMs(10_000)
        )

        assertEquals(10.seconds, metrics.wallTime)
    }

    @Test
    fun `The metrics wallTime Duration representation setter works as expected`() {
        val metrics = executionMetrics {
            wallTime = 10.seconds
        }

        assertEquals(10_000, metrics.wallTimeMs)
    }

    @Test
    fun `The metrics memory ByteSize representation getter works as expected`() {
        val metrics = executionMetrics {
            memoryBytes = 10_000
        }

        assertEquals(10.KB, metrics.memory)
    }

    @Test
    fun `The metrics memory ByteSize representation setter works as expected`() {
        val metrics = executionMetrics {
            memory = 10.KB
        }

        assertEquals(10_000, metrics.memoryBytes)
    }

    @Test
    fun `The metrics disk ByteSize representation getter works as expected`() {
        val metrics = executionMetrics {
            diskBytes = 10_000
        }

        assertEquals(10.KB, metrics.disk)
    }

    @Test
    fun `The metrics disk ByteSize representation setter works as expected`() {
        val metrics = executionMetrics {
            disk = 10.KB
        }

        assertEquals(10_000, metrics.diskBytes)
    }

    @Test
    fun `The metrics output ByteSize representation getter works as expected`() {
        val metrics = executionMetrics {
            outputBytes = 10_000
        }

        assertEquals(10.KB, metrics.output)
    }

    @Test
    fun `The metrics output ByteSize representation setter works as expected`() {
        val metrics = executionMetrics {
            output = 10.KB
        }

        assertEquals(10_000, metrics.outputBytes)
    }
}

/**
 * This filename cannot just be "OutputRequest.kt" due to a limitation or
 * bug in the Kotlin JVM compiler. The explanation for this lies in how Kotlin
 * compiles top-level non-class definitions like these extension properties,
 * together with the specific structure of the generated protobuf Kotlin code.
 *
 * In short, top-level non-class declarations in a file Foo.kt are compiled to
 * JVM to be a static member of a "shadow class" called FooKt. Additionally,
 * protobuf generates Kotlin-specific code for a message "Foo" in a class FooKt,
 * such as the DSL. Consequently, when naming this file "OutputRequest.kt",
 * this will create a class name conflict at compile time. The produced error
 * is not very clear about this and will lead you on a stray path.
 */
package nl.rug.protospec.judgement.v1.extensions

import nl.rug.digitallab.common.kotlin.quantities.wrapByteSize
import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.protospec.judgement.v1.OutputRequest
import nl.rug.protospec.judgement.v1.OutputRequestKt
import nl.rug.protospec.judgement.v1.utils.wrapUri
import java.net.URI


// This will be valid in Kotlin 2.1:
// https://youtrack.jetbrains.com/issue/KT-8575/Support-Java-synthetic-property-references
//
// val OutputRequest.maxSize by byteSizeDelegate(OutputRequest::maxSizeBytes)

// Exposes the underlying maxSizeBytes as `maxSize: ByteSize` to users of the library.
val OutputRequest.maxSize get() = this.maxSizeBytes.B
var OutputRequestKt.Dsl.maxSize by wrapByteSize(OutputRequestKt.Dsl::maxSizeBytes)

// Exposes the underlying uriAsString as `uri: URI` to users of the library.
val OutputRequest.uri get() = URI(this.uriString)
var OutputRequestKt.Dsl.uri by wrapUri(OutputRequestKt.Dsl::uriString)

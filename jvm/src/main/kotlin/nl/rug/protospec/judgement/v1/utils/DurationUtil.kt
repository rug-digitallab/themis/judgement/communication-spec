package nl.rug.protospec.judgement.v1.utils

import nl.rug.digitallab.common.kotlin.helpers.delegate.MutableConversionDelegate
import kotlin.reflect.KMutableProperty1
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds

/**
 * Provides a delegate that wraps the provided [Int] field in a [Duration]
 * instance. Any reads and writes will be directed to the source [Int] field.
 *
 * @param backingProperty The backing [Long] property
 */
fun <T> wrapDuration(
    backingProperty: KMutableProperty1<T, Int>,
) = MutableConversionDelegate(
    backingProperty,
    {integer: Int -> integer.milliseconds },                       // Convert from wallTimeMs to wallTime (Getter)
    {duration: Duration -> duration.inWholeMilliseconds.toInt() }, // Convert from wallTime to wallTimeMs (Setter)
)

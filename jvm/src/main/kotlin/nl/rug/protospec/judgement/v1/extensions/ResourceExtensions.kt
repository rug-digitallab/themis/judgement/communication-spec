package nl.rug.protospec.judgement.v1.extensions

import nl.rug.protospec.judgement.v1.Resource
import nl.rug.protospec.judgement.v1.ResourceKt
import nl.rug.protospec.judgement.v1.utils.wrapUri
import java.net.URI

// Exposes the underlying uriAsString as `uri: URI` to users of the library.
val Resource.uri get() = URI(this.uriString)
var ResourceKt.Dsl.uri by wrapUri(ResourceKt.Dsl::uriString)

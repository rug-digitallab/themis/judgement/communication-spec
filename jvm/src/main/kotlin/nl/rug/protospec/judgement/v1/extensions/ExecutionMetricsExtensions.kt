/**
 * This filename cannot just be "ExecutionMetrics.kt" due to a limitation or
 * bug in the Kotlin JVM compiler. The explanation for this lies in how Kotlin
 * compiles top-level non-class definitions like these extension properties,
 * together with the specific structure of the generated protobuf Kotlin code.
 *
 * In short, top-level non-class declarations in a file Foo.kt are compiled to
 * JVM to be a static member of a "shadow class" called FooKt. Additionally,
 * protobuf generates Kotlin-specific code for a message "Foo" in a class FooKt,
 * such as the DSL. Consequently, when naming this file "ExecutionMetrics.kt",
 * this will create a class name conflict at compile time. The produced error
 * is not very clear about this and will lead you on a stray path.
 */
package nl.rug.protospec.judgement.v1.extensions

import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.digitallab.common.kotlin.quantities.wrapByteSize
import nl.rug.protospec.judgement.v1.ExecutionMetrics
import nl.rug.protospec.judgement.v1.ExecutionMetricsKt
import nl.rug.protospec.judgement.v1.utils.wrapDuration
import kotlin.time.Duration.Companion.milliseconds

// Exposes the underlying wallTimeMs as `wallTime: Duration` to users of the library.
val ExecutionMetrics.wallTime get() = this.wallTimeMs.milliseconds
var ExecutionMetricsKt.Dsl.wallTime by wrapDuration(ExecutionMetricsKt.Dsl::wallTimeMs)

// Exposes the underlying memoryBytes as `memory: ByteSize` to users of the library.
val ExecutionMetrics.memory get() = this.memoryBytes.B
var ExecutionMetricsKt.Dsl.memory by wrapByteSize(ExecutionMetricsKt.Dsl::memoryBytes)

// Exposes the underlying diskBytes as `disk: ByteSize` to users of the library.
val ExecutionMetrics.disk get() = this.diskBytes.B
var ExecutionMetricsKt.Dsl.disk by wrapByteSize(ExecutionMetricsKt.Dsl::diskBytes)

// Exposes the underlying outputBytes as `output: ByteSize` to users of the library.
val ExecutionMetrics.output get() = this.outputBytes.B
var ExecutionMetricsKt.Dsl.output by wrapByteSize(ExecutionMetricsKt.Dsl::outputBytes)

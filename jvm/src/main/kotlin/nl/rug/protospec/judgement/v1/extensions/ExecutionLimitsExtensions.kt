/**
 * This filename cannot just be "ExecutionLimits.kt" due to a limitation or
 * bug in the Kotlin JVM compiler. The explanation for this lies in how Kotlin
 * compiles top-level non-class definitions like these extension properties,
 * together with the specific structure of the generated protobuf Kotlin code.
 *
 * In short, top-level non-class declarations in a file Foo.kt are compiled to
 * JVM to be a static member of a "shadow class" called FooKt. Additionally,
 * protobuf generates Kotlin-specific code for a message "Foo" in a class FooKt,
 * such as the DSL. Consequently, when naming this file "ExecutionLimits.kt",
 * this will create a class name conflict at compile time. The produced error
 * is not very clear about this and will lead you on a stray path.
 */
package nl.rug.protospec.judgement.v1.extensions

import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.digitallab.common.kotlin.quantities.wrapByteSize
import nl.rug.protospec.judgement.v1.ExecutionLimits
import nl.rug.protospec.judgement.v1.ExecutionLimitsKt
import nl.rug.protospec.judgement.v1.utils.wrapDuration
import kotlin.time.Duration.Companion.milliseconds

// Exposes the underlying wallTimeMs as `wallTime: Duration` to users of the library.
val ExecutionLimits.wallTime get() = this.wallTimeMs.milliseconds
var ExecutionLimitsKt.Dsl.wallTime by wrapDuration(ExecutionLimitsKt.Dsl::wallTimeMs)

// Exposes the underlying memoryBytes as `memory: ByteSize` to users of the library.
val ExecutionLimits.memory get() = this.memoryBytes.B
var ExecutionLimitsKt.Dsl.memory by wrapByteSize(ExecutionLimitsKt.Dsl::memoryBytes)

// Exposes the underlying diskBytes as `disk: ByteSize` to users of the library.
val ExecutionLimits.disk get() = this.diskBytes.B
var ExecutionLimitsKt.Dsl.disk by wrapByteSize(ExecutionLimitsKt.Dsl::diskBytes)

// Exposes the underlying outputBytes as `output: ByteSize` to users of the library.
val ExecutionLimits.output get() = this.outputBytes.B
var ExecutionLimitsKt.Dsl.output by wrapByteSize(ExecutionLimitsKt.Dsl::outputBytes)

package nl.rug.protospec.judgement.v1.utils

import nl.rug.digitallab.common.kotlin.helpers.delegate.MutableConversionDelegate
import java.net.URI
import kotlin.reflect.KMutableProperty1

/**
 * Provides a delegate that wraps the provided [String] field in a [URI]
 * instance. Any reads and writes will be directed to the source [String] field.
 *
 * @param backingProperty The backing [String] property
 */
fun <T> wrapUri(
    backingProperty: KMutableProperty1<T, String>,
) = MutableConversionDelegate(
    backingProperty,
    {uriAsString: String -> URI(uriAsString) },  // Convert from String to URI (Getter)
    {uri: URI -> uri.toString() },               // Convert from URI to String (Setter)
)

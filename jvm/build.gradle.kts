import com.google.protobuf.gradle.id
import com.google.protobuf.gradle.remove

val protobufVersion: String by project

plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.library")
    id("com.google.protobuf")
}

dependencies {
    api("io.quarkus:quarkus-grpc")
    api("com.google.protobuf:protobuf-kotlin:${protobufVersion}")
}

quarkus {
    quarkusBuildProperties.put("quarkus.grpc.codegen.proto-directory", "${project.projectDir}/../proto")
}

tasks.withType<Jar>() {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}

/**
 * This code will become redundant once Quarkus implements support
 * for Kotlin code generation out of the box, see the related
 * issue: https://github.com/quarkusio/quarkus/issues/39127
 *
 * We set up the normal protobuf integration to generate only
 * the Kotlin extensions and not the base Java code - the Java
 * code is already being through the Quarkus route.
 */
sourceSets {
    main {
        proto {
            srcDir("${project.projectDir}/../proto")
        }
    }
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:${protobufVersion}"
    }

    generateProtoTasks {
        all().configureEach {
            builtins {
                id("kotlin")
                remove("java")
            }
        }
    }
}

rootProject.name = "communication-spec"

pluginManagement {
    plugins {
        val digitalLabGradlePluginVersion: String by settings
        val protobufPluginVersion: String by settings

        id("nl.rug.digitallab.gradle.plugin.quarkus.library") version digitalLabGradlePluginVersion
        id("com.google.protobuf") version protobufPluginVersion
    }

    repositories {
        maven("https://gitlab.com/api/v4/groups/65954571/-/packages/maven") // Digital Lab Gradle Plugin
        gradlePluginPortal()
    }
}

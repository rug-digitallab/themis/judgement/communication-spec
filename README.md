# Judgement communication spec
The judgement communication spec provides the specification for communication between the core of Themis and the runtimes that wil execute actions such as compilers and testcases. Runtimes will implement this specification as a server handling gRPC requests, whle the Themis core will serve as the gRPC client, sending requests to the runtimes to run actions.

# Prerequisites
- Gradle
- JDK21 or newer

# Usage
The libary is made available at https://gitlab.com/rug-digitallab/products/themis/judgement/communication-spec/-/packages. One can include the library into their project as follows (Written in Gradle using Kotlin DSL):
```kotlin
dependencies {
    implementation("nl.rug.digitallab.themis.judgement:judgement-communication-spec:<version>")
}
```
Once that has been done, one can simply follow the Quarkus gRPC guide: https://quarkus.io/guides/grpc-getting-started.

## Server
One can implement a server as follows:
```kotlin
@GrpcService
class ActionService : ActionRunner {
    @OptIn(ExperimentalCoroutinesApi::class, DelicateCoroutinesApi::class)
    override fun runAction(request: RunActionRequest?): Uni<RunActionResponse> {

    }
}

```

## Client
### With Quarkus CDI
One can implement a client using CDI as follows:
```kotlin
@ApplicationScoped
class GrpcClient {
    @GrpcClient
    private lateinit var runtime: ActionRunner

    suspend fun runAction(request: RunActionRequest): RunActionResponse
        = withContext(Dispatchers.IO) {
            runtime.runAction(request).await().atMost(<TimeToWait>)
        }
}

```

### Without Quarkus CDI
One can implement a client without CDI as follows:
```kotlin
fun callGrpc(request: RunActionRequest): RunActionResponse {
    val channel = ManagedChannelBuilder.forAddress(<host>, <port>)
        .usePlaintext()
        .build()

    try {
        val stub = ActionRunnerGrpc.newBlockingStub(channel)

        return stub.runAction(request)
    } finally {
        channel.shutdown()
    }
}
```

# Implementation
## Code structure
The core part of the repository is the `proto` folder. This folder contains the actual specification of the communication spec. This repository is designed such that compiling the protobuf is the responsibility of this repository, and not the projects using the specification. As such, other folders (like `JVM`) will deal with generating a library implementing the specification in the respective language, which can then be pulled by the projects from a Maven repository.

## Protobuf
Everything in the communication spec is about one gRPC call: `RunAction`. The function simply takes a `RunActionRequest` and returns a `RunActionResponse`. The `RunActionRequest` contains all the data Themis core should send to the runtimes, while `RunActionResponse` will contain all the data that the runtimes should send back as a response.

## JVM
Since Themis makes use of Quarkus, and Quarkus has a tight Quarkus-specific integration with gRPC, we need to generate a Quarkus-compatible library for the projects. The JVM library for the communication spec is specifically intended to work with Quarkus, and no other gRPC approach. Furthermore, it also includes Kotlin sources, such that Kotlin-native syntax can be used by Kotlin projects, which significantly cleans up code interacting with the communication spec if used properly.
